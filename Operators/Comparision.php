<?php

namespace App\Operators;

class Comparision
{
    /**
     * Property type declaration will be added when support for 7.4 is available.
     * @var int $x, $y
     */
    private $x, $y;

    /**
     * Property type declaration will be added when support for 7.4 is available.
     * @var string $string
     */
    private $string;

    /**
     * Conditional constructor.
     *
     * @param int    $x
     * @param int    $y
     * @param string $string
     *
     * @return $this
     */
    public function __construct(int $x, int $y, string $string)
    {
        $this->x        = $x;
        $this->y        = $y;
        $this->string   = $string;

        return $this;
    }

    /**
     * The equal operator returns true if ($X is equal to $Y)
     * @return string
     */
    public function equal()
    {
        print 'Executing equal (==) operator' . PHP_EOL;

        if ($this->x == $this->y):
            $result = 'TRUE $X('. $this->x .') is equal to $Y('. $this->y .')';
        else:
            $result = 'FALSE $X('. $this->x .') is not equal to $Y('. $this->y .')';
        endif;

        return $result;
    }

    /**
     * The not equal operator returns true if ($X is not equal to $Y)
     * @return string
     */
    public function notEqual()
    {
        print 'Executing not equal (!=) operator' . PHP_EOL;

        if ($this->x != $this->y):
            $result = 'TRUE $X('. $this->x .') is not equal to $Y('. $this->y .')';
        else:
            $result = 'FALSE $X('. $this->x .') is equal to $Y('. $this->y .')';
        endif;

        return $result;
    }

    /**
     * The identical operator returns true if $X is equal to $Z, and they are of the same type
     * @return string
     */
    public function identical()
    {
        print 'Executing identical (===) operator' . PHP_EOL;

        if ($this->x === $this->y):
            $result = 'TRUE $X('. $this->x .') is identical to $Y('. $this->y .')';
        else:
            $result = 'FALSE $X('. $this->x .') is not identical to $Y('. $this->y .')';
        endif;

        return $result;
    }

    /**
     * The not identical operator returns true if $X is not equal to $Y, or they are not of the same type
     * @return string
     */
    public function notIdentical()
    {
        print 'Executing not identical (!==) operator' . PHP_EOL;

        if ($this->x !== $this->y):
            $result = 'TRUE $X('. $this->x .') is not identical to $Y('. $this->y .')';
        else:
            $result = 'FALSE $X('. $this->x .') is identical to $Y('. $this->y .')';
        endif;

        return $result;
    }

    /**
     * The greater than operator returns true if $X is greater than $Y
     * @return string
     */
    public function greaterThan()
    {
        print 'Executing greater than (>) operator' . PHP_EOL;

        if ($this->x > $this->y):
            $result = 'TRUE $X('. $this->x .') is greater than $Y('. $this->y .')';
        else:
            $result = 'FALSE $X('. $this->x .') is not greater than $Y('. $this->y .')';
        endif;

        return $result;
    }

    /**
     * The less than operator returns true if $X is less than $Y
     * @return string
     */
    public function lessThan()
    {
        print 'Executing less than (<) operator' . PHP_EOL;

        if ($this->x < $this->y):
            $result = 'TRUE $X('. $this->x .') is less than $Y('. $this->y .')';
        else:
            $result = 'FALSE $X('. $this->x .') is not less than $Y('. $this->y .')';
        endif;

        return $result;
    }

    /**
     * The greater than or equal to operator returns true if $X is greater than or equal to $Y
     * @return string
     */
    public function greaterThanOrEqualTo()
    {
        print 'Executing greater than or equal to (>=) operator' . PHP_EOL;

        if ($this->x >= $this->y):
            $result = 'TRUE $X('. $this->x .') is greater than or equal to $Y('. $this->y .')';
        else:
            $result = 'FALSE $X('. $this->x .') is not greater than or equal to $Y('. $this->y .')';
        endif;

        return $result;
    }

    /**
     * The less than or equal to operator returns true if $X is less than or equal to $Y
     * @return string
     */
    public function lessThanOrEqualTo()
    {
        print 'Executing less than or equal to (<=) operator' . PHP_EOL;

        if ($this->x <= $this->y):
            $result = 'TRUE $X('. $this->x .') is less than or equal to $Y('. $this->y .')';
        else:
            $result = 'TRUE $X('. $this->x .') is not less than or equal to $Y('. $this->y .')';
        endif;

        return $result;
    }

    /**
     * The spaceship operator is a simple three sided operator, it checks two values and results either one in three
     * outcomes (-1, 0 or 1).
     *
     * For Example:
     * It makes use of three separate operators combined into one elegant one. Often the result of this operator is used
     * in a switch statement like shown in the example below.
     *
     *              -1  0  1
     *           5   <  =  >   10
     *
     * The output of the statement above would be (-1) because 5 is smaller than 10.
     *
     * @return string
     */
    public function spaceship()
    {
        print 'Executing spaceship (<=>) operator' . PHP_EOL;

        switch ($this->x <=> $this->y):
            case -1:
                $results = 'I\'ll be shown if $X('. $this->x .') is smaller than $Y('. $this->y .')';
                break;
            case 0:
                $results = 'I\'ll be shown if $X('. $this->x .') is equal to $Y('. $this->y .')';
                break;
            case 1:
                $results = 'I\'ll be shown if $X('. $this->x .') is greater than $Y('. $this->y .')';
                break;
            default:
                $results = 'The default statement is not reachable since there are only three possible outcomes';
        endswitch;

        return  $results;
    }
}
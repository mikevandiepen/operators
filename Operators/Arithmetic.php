<?php

namespace App\Operators;

class Arithmetic
{
    /**
     * Property type declaration will be added when support for 7.4 is available.
     * @var int $x, $y
     */
    private $x, $y;

    /**
     * Property type declaration will be added when support for 7.4 is available.
     * @var string $string
     */
    private $string;

    /**
     * Conditional constructor.
     *
     * @param int    $x
     * @param int    $y
     * @param string $string
     *
     * @return $this
     */
    public function __construct(int $x, int $y, string $string)
    {
        $this->x        = $x;
        $this->y        = $y;
        $this->string   = $string;

        return $this;
    }

    /**
     * Sum of $X and $Y
     * @return string
     */
    public function addition()
    {
        return '$X('. $this->x .') + $Y('. $this->y .') = ' . ($this->x + $this->y);
    }

    /**
     * Difference of $X and $Y
     * @return string
     */
    public function subtraction()
    {
        return '$X('. $this->x .') - $Y('. $this->y .') = ' . ($this->x - $this->y);
    }

    /**
     * Product of $X and $Y
     * @return string
     */
    public function multiplication()
    {
        return '$X('. $this->x .') * $Y('. $this->y .') = ' . ($this->x * $this->y);
    }

    /**
     * Quotient of $X and $Y
     * @return string
     */
    public function division()
    {
        return '$X('. $this->x .') / $Y('. $this->y .') = ' . ($this->x / $this->y);
    }

    /**
     * Remainder of $X divided by $Y
     * @return string
     */
    public function modulus()
    {
        print 'Remainder of $X('. $this->x .') divided by $Y('. $this->y .')' . PHP_EOL;

        return '$X('. $this->x .') % $Y('. $this->y .') = ' . ($this->x % $this->y);
    }

    /**
     * Result of raising $X to the $Y'th power
     * @return string
     */
    public function exponentiation()
    {
        print '$X to te power of $Y\'th ('. $this->x .'^'. $this->y .')' . PHP_EOL;

        return '$X('. $this->x .') ** $Y('. $this->y .') = ' . ($this->x ** $this->y);
    }
}
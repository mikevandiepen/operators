<?php

namespace App\Operators;

class Conditional
{
    /**
     * Property type declaration will be added when support for 7.4 is available.
     * @var int $x, $y
     */
    private $x, $y;

    /**
     * Property type declaration will be added when support for 7.4 is available.
     * @var string $string
     */
    private $string;

    /**
     * Conditional constructor.
     *
     * @param int    $x
     * @param int    $y
     * @param string $string
     *
     * @return $this
     */
    public function __construct(int $x, int $y, string $string)
    {
        $this->x        = $x;
        $this->y        = $y;
        $this->string   = $string;

        return $this;
    }

    /**
     * The ternary operator is a shorthand 'if() { doSomething() } else { doSomethingElse() }' statement.
     * @return string
     */
    public function ternary()
    {
        return ($this->x < $this->y) // Statement
            ? 'TRUE, $X('. $this->x .') is smaller than $Y('. $this->y .')'         // The results if the statement is true
            : 'False, $X('. $this->x .') is not smaller than $Y('. $this->y .')';   // The results if the statement is false
    }

    /**
     * The coalesce operator is known as 'fallback operator'. This operator returns the second statement if the argument
     * before the '??' is null.
     * @return string
     */
    public function coalesce()
    {
        return $this->string ?? 'I\'ll be shown if the value of $string is null.';
    }
}
<?php

use App\Facades\OperatorFacade;

require_once ('./Facades/OperatorFacade.php');
require_once ('./Operators/Arithmetic.php');
require_once ('./Operators/Conditional.php');
require_once ('./Operators/Comparision.php');

// Parameter config
$operator = new OperatorFacade(3, 50, 'I am a string');

// Operator types
//-----------------------
// - Arithmetic
// - Conditional
// - Comparision

// Operators options
//-----------------------
// --- Arithmetic ---
//      - $operator->addition()
//      - $operator->subtraction()
//      - $operator->multiplication()
//      - $operator->division()
//      - $operator->modulus()
//      - $operator->exponentiation()
// --- Conditional ---
//      - $operator->equal()
//      - $operator->notEqual()
//      - $operator->identical()
//      - $operator->notIdentical()
//      - $operator->greaterThan()
//      - $operator->lessThan()
//      - $operator->greaterThanOrEqualTo()
//      - $operator->lessThanOrEqualTo()
// --- Comparision ---
//      - $operator->spaceship()
//      - $operator->ternary()
//      - $operator->coalesce()

// Play around with the operators here...
print_r(PHP_EOL . '--------------------[ TEST ]--------------------' . PHP_EOL);
print_r($operator->ternary()); // Choose your operator
print_r(PHP_EOL . '------------------------------------------------' . PHP_EOL);
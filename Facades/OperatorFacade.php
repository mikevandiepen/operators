<?php

namespace App\Facades;

use App\Operators\Arithmetic;
use App\Operators\Comparision;
use App\Operators\Conditional;

class OperatorFacade
{
    /**
     * Different types of operators
     * @var object
     */
    private
        $arithmetic,
        $comparision,
        $conditional;

    /**
     * OperatorFacade constructor.
     *
     * @param int    $x
     * @param int    $y
     * @param string $string
     */
    public function __construct(int $x, int $y, string $string)
    {
        $this->arithmetic   = new Arithmetic($x, $y, $string);
        $this->comparision  = new Comparision($x, $y, $string);
        $this->conditional  = new Conditional($x, $y, $string);
    }

    //-----------------------------------------------------
    //              Arithmetic Operators
    //-----------------------------------------------------

    public function addition()
    {
        return $this->arithmetic->addition();
    }

    public function subtraction()
    {
        return $this->arithmetic->subtraction();
    }

    public function multiplication()
    {
        return $this->arithmetic->multiplication();
    }

    public function division()
    {
        return $this->arithmetic->division();
    }

    public function modulus()
    {
        return $this->arithmetic->modulus();
    }

    public function exponentiation()
    {
        return $this->arithmetic->exponentiation();
    }

    //-----------------------------------------------------
    //              Comparision Operators
    //-----------------------------------------------------

    public function equal()
    {
        return $this->comparision->equal();
    }

    public function notEqual()
    {
        return $this->comparision->notEqual();
    }

    public function identical()
    {
        return $this->comparision->identical();
    }

    public function notIdentical()
    {
        return $this->comparision->notIdentical();
    }

    public function greaterThan()
    {
        return $this->comparision->greaterThan();
    }

    public function lessThan()
    {
        return $this->comparision->lessThan();
    }

    public function greaterThanOrEqualTo()
    {
        return $this->comparision->greaterThanOrEqualTo();
    }

    public function lessThanOrEqualTo()
    {
        return $this->comparision->lessThanOrEqualTo();
    }

    //-----------------------------------------------------
    //              Conditional Operators
    //-----------------------------------------------------

    public function ternary()
    {
        return $this->conditional->ternary();
    }

    public function coalesce()
    {
        return $this->conditional->coalesce();
    }

    public function spaceship()
    {
        return $this->comparision->spaceship();
    }
}